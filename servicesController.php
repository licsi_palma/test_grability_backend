<?php

/**
 * @author Licsileth Palma
 * @copyright 2016
 */
 
define('APPDIR', dirname(__FILE__) );
define('DS', DIRECTORY_SEPARATOR );

include_once (APPDIR.DS.'mainController.php');
 
class servicesController extends MainController {

	public function post_confirm(){
    
    //Asignamos variable service_id del formulario
    $id = Input::get(service_id);
	
	//Procedemos a buscar el Servicio	
    $servicio=Service::find($id);
    
	//Procedemos a validar el Servicio
    if($servicio != NULL){
        
		if($servicio->status_id == $this::COD_STATUS_INACTIVO){
            return Response::json(array('error'=>$this::COD_ERROR_STATUS_NO_ASIGNADO));
        }
		
        if($servicio->driver_id == NULL && $servicio->status_id == $this::COD_STATUS_DRIVER_NO_ASIGNADO){
            $servicio=Service::update($id,array(
                        'driver_id'=>Input::get('driver:id'),
                        'status_id'=>$this::COD_STATUS_DRIVER_ASIGNADO
            ));
			
            Driver::update(Input::get('driver_id'),array(
                "available"=>'0'            
            ));
			
            $driverTmp=Driver::find(Input::get('driver_id'));
            Service::update($id, array(
                'car_id'=>$driverTmp->car_id     
            ));
			
            //Notificar a usuario!!
            $pushMessage = $this::MSG_SERVICIO_CONFIRMADO;
            $push = Push::make();  
			
            if($servicio->user->uuid == ''){
                return Response::json(array('error'=> $this::COD_ERROR_USUARIO_NO_ASIGNADO));
            }else{
				//iPhone
				if($servicio->user->type == $this::COD_SERVICES_TYPE_IPHONE){
					$result = $push->ios($servicio->user->uuid,$pushMessage,1,'honk.wav','Open',array('serviceId'=>$servicio->id));
				}else{
					$result= $push->android2($servicio->user->$pushMessage,1,'default','Open',array('serviceId'=>$servcio->id));
				}
			}
        } else{
            return Response::json(array('error'=>$this::COD_ERROR_DRIVER_NO_ASIGNADO));
        }    
    }else{
        return Response::json(array('error'=>$this::COD_ERROR_SERVICES_NULL));
    }
 }

 
 

//Codigo Original
public function post_confirm_Original(){
    
    
    $id=Input::get(service_id);
    $servicio=Service::find($id);
    //dd($servicio);
    if($servicio!=NULL){
        if($servicio->status_id=='6'){
            return Response::json(array('error'=>'2'));
        }
        if($servicio->driver_id==NULL && $servicio->status_id=='1'){
            $servicio=Service::update($id,array(
                        'driver_id'=>Input::get('driver:id'),
                        'status_id'=>'2'
                            //Up Carro
                            //,'pwd'=>md5(Input::get('pwd'))
            
            ));
            Driver::update(Input::get('driver_id'),array(
                "available"=>'0'
            
            ));
            $driverTmp=Driver::find(Input::get('driver_id'));
            Service::update($id, array(
                'car_id'=>$driverTmp->car_id
                    //Up carro
                    //,'pwd'=>md5(Input::get('pwd'))
            
            ));
            //Notificar a usuario!!
            $pushMessage='Tu servicio ha sido confirmado!';
            /*$servicio=Service::find($id));
             $push=Push::make();
             if($servicio->user->type=='1'){//iPhone
             $pushAns=$push->ios($servicio->user->uuid,$pushMessage);   
             }else{
             $pushAns=$push->android($servicio->user->uuid,$pushMessage);  
             }*/
            $servicio=Service::find($id);
            $push=Push::make();
            if($servicio->user->uuid==''){
                return Response::json(array('error'=>'0'));
            }
            if($servicio->user->type=='1'){//iPhone
                $result=$push->ios($servicio->user->uuid,$pushMessage,1,'honk.wav','Open',array('serviceId'=>$servicio->id));
            }else{
                $result=$push->android2($servicio->user->$pushMessage,1,'default','Open',array('serviceId'=>$servcio->id));
            }
            return Response::json(array('error'=>'0'));               
        } else{
            return Response::json(array('error'=>'1'));
        }    
    }else{
        return Response::json(array('error'=>'3'));
    }
 }

}

 

 
 
 
 
 
 


?>