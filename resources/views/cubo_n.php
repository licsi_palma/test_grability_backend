<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title> Cube Summation </title>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/operation.js"></script>
		<script type="text/javascript" src="js/modal.windows.js"></script>  
    </head>
    <body>
	<form id="Form_Cubo" action="SaveCubo" method="post">    
    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
    	
        <div id="Div_Control_Casos">
              <table>
                  <tr>
                        <td>
                               Indique el Numero de Casos: 
                        </td>
                        <td>
                                <input type="text" name="Txt_NumeroCasos" id="Txt_NumeroCasos" value="<?php echo $NumeroCasos; ?>" readonly="true" />
                        </td>
                  </tr>
              </table>  
        </div>
        
        
    
        <label id="lbl_iteraccion_info"><?php echo $Iteracion_Info; ?></label>
        
        <div id="Div_Control_DefineMatriz">
              <table>
                  <tr>
                        <td>
                               Define Matriz y Numero de Operaciones separados por un espacio (Ejemplo: 4 5) 
                        </td>
                        <td>
                                <input type="text" name="Txt_MatrizAndOpreaciones" id="Txt_MatrizAndOpreaciones" value="" />
                        </td>                     
                  </tr> 
              </table>  
        </div>
        
        <div id="contenedor_operacion">
            <div class="added">
                
            </div>
        </div>
        <div id="Div_Control_Save" style="display: none;">
              <table>
                  <tr>          
                        <td>
                            <input type="submit" id="Btn_Salvar_Operacion" name="Btn_Salvar_Operacion" Value="Salvar Operaciones" /> 
                        </td> 
                  </tr> 
              </table>  
        </div>        
        <input type="hidden" name="Txt_All_Operaciones" id="Txt_All_Operaciones" value="" />
        <input type="hidden" name="Txt_N_Iteracion" id="Txt_N_Iteracion" value="<?php echo $Iteracion_Number; ?>" />
    </form>	       
    </body>    
</html>