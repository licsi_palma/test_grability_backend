<?php

/**
 * @author Licsileth Palma
 * @copyright 2016
 */

 class MainController{
	
	/** Constantes de Validacion */

	const COD_STATUS_INACTIVO = '6';

	const COD_STATUS_DRIVER_NO_ASIGNADO = '1';

	const COD_STATUS_DRIVER_ASIGNADO = '2';
	
	
	const COD_SERVICES_TYPE_IPHONE = '1';
	
	const COD_SERVICES_TYPE_ANDROID = '2';
	
	 
	/** Constantes de Mensajes asociados al error */

			
	//Codigo de Error servicio nulo
	const COD_ERROR_USUARIO_NO_ASIGNADO = '0';
	
	//Codigo de Error Driver no asignado
	const COD_ERROR_DRIVER_NO_ASIGNADO = '1';
	
	//Codigo de Error servicio nulo
	const COD_ERROR_STATUS_NO_ASIGNADO = '2';
	
	//Codigo de Error servicio nulo
	const COD_ERROR_SERVICES_NULL = '3';
	
	
	 /** Constantes de Mensajes  */
 
	const MSG_SERVICIO_CONFIRMADO = 'Tu servicio ha sido confirmado!';
	
 }
 
 ?>