<?php

namespace App;

class Matriz    
{
    
    public $m, $n, $matriz;

    function __construct($n, $m)
    {
        $this->m = $m;
        $this->n = $n;
        $this->createMatriz($n);
    }

    private function createMatriz($n)
    {
        for ($i = 0; $i <= $n; $i++) {
            for ($j = 0; $j <= $n; $j++) {
                for ($k = 0; $k <= $n; $k++) {
                    $this->matriz[$i][$j][$k] = 0;
                    //echo "Formato Matriz Formada ".$i." ".$j." ".$k." con valor: ".  $this->matriz[$i][$j][$k]. "<br>";
                    
                }
            }
        }
    }

    public function updateValue($x, $y, $z, $value)
    {
        $this->matriz[$x][$y][$z] = $value;
    }

    public function query($x1, $y1, $z1, $x2, $y2, $z2)
    {
        $sum = 0;
        for ($i = $x1; $i <= $x2; $i++) {
            for ($j = $y1; $j <= $y2; $j++) {
                for ($k = $z1; $k <= $z2; $k++) {
                    $sum += $this->matriz[$i][$j][$k];
                }
            }
        }
        return $sum;
    }

    public function getM()
    {
        return $this->m;
    }

    public function getMatriz()
    {
        return $this->matriz;
    }

    public function getN()
    {
        return $this->n;
    }  
}
