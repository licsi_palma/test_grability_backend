<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{    
    public function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    private function returnValue($key)
    {
        $value = null;
        if (isset($_SESSION[$key])) {
            $value = $_SESSION[$key];

        }
        return $value;
    }
    
    public function getMatriz()
    {
        return $this->returnValue('matriz');
    }

    public function getActions()
    {
        return $this->returnValue('actions');

    }
    
    public function getResults()
    {
        return $this->returnValue('results');
    }
    
    public function getIndexResults()
    {
        return $this->returnValue('indexResult');
    }

    public function setMatriz($matriz)
    {
        $_SESSION['matriz'] = $matriz;
    }
    
    public function setActions($actions)
    {
        $_SESSION['actions'] = $actions;
    }
    
    public function setResults($results)
    {
        $_SESSION['results'] = $results;
    }
    
    public function setIndexResults($indexResult)
    {
        $_SESSION['indexResult'] = $indexResult;
    }

    public function deleteSession(){
        session_destroy();
    }
    
}
