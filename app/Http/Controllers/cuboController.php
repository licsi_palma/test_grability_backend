<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Results;
use App\Matriz;
use App\Info;

class cuboController extends Controller
{
    
    //controlador
    public function CreateCubo (Request $request){
           
           $Info = new Info();       
           $Info->deleteSession();
           return view('cubo');
    }
    
    //controlador
    public function SaveCubo (Request $request){
           
            $NumeroCasos_Request = $request->input('Txt_NumeroCasos');
            echo "<br> Numero de Casos: ". $NumeroCasos_Request;
            
            $Matriz_and_Operations = $request->input('Txt_MatrizAndOpreaciones');
            $Matriz_and_Operations_expl = explode(" ",$Matriz_and_Operations); 
            
            $Matriz_Dimension = $Matriz_and_Operations_expl[0]; 
            $Matriz_Operations = $Matriz_and_Operations_expl[1];           
           
            //Procedemos a crear la Matriz 
            
            $Info = new Info();       
            //$Info->deleteSession();     
            $matriz = new Matriz($Matriz_Dimension, $Matriz_Operations);
            
            $Info->setMatriz($matriz);
            $Info->setActions(0);
            
            $Results = $Info->getResults();
            $IndexResult = 0;
            
            if(!isset($Results)){                
                $Results = new Results();                
            }else{
                $Results = $Info->getResults();
                $IndexResult = $Results->getIndexResult();
            }
                        
            
            //Procedemos a procesar las operaciones
            $All_Operations = $request->input('Txt_All_Operaciones');
            $Array_Operations = explode("&",$All_Operations);
            
                        
            //Procedemos a setear las operaciones
            for($index_control = 0; $index_control < count($Array_Operations); $index_control++){
                                
                $Process_Operation = explode(" ", $Array_Operations[$index_control]);                
                $Operation_Action = $Process_Operation[0];
                
                if(strtolower($Operation_Action) == "update"){
                    $X_Value =  $Process_Operation[1];
                    $Y_Value =  $Process_Operation[2];
                    $Z_Value =  $Process_Operation[3];                    
                    $W_Value =  $Process_Operation[4];
                    
                    $this->UpdateOperation($X_Value, $Y_Value, $Z_Value, $W_Value);
                                        
                }elseif(strtolower($Operation_Action) == "query"){
                    
                    $X1_Value =  $Process_Operation[1];
                    $Y1_Value =  $Process_Operation[2];
                    $Z1_Value =  $Process_Operation[3];                    
                    
                    $X2_Value =  $Process_Operation[4];
                    $Y2_Value =  $Process_Operation[5];
                    $Z2_Value =  $Process_Operation[6];
                    
                    $OperationResult = $this->QueryOperation($X1_Value, $Y1_Value, $Z1_Value, $X2_Value, $Y2_Value, $Z2_Value);
                    $Results->addValue($IndexResult, $OperationResult);
                    
                    $IndexResult++;
                    $Results->setIndexResult($IndexResult);
                    
                }else{
                    echo "operacion invalida no procesada en UI";   
                }                      
            }
            
           $Info->setResults($Results);
            
           $Array_Parametros = array();
           $Array_Parametros["NumeroCasos"] = $NumeroCasos_Request;
           
           $Iteracion_Number = $request->input('Txt_N_Iteracion');
           
           if($NumeroCasos_Request == $Iteracion_Number){
                return $this->LoadResults();            
           }else{
                $Iteracion_Number++;                
           }
           
           $Array_Parametros["Iteracion_Info"] = "Iteracion_InfoIteraccion N&uacute;mero $Iteracion_Number  de:  $NumeroCasos_Request";           
           $Array_Parametros["Iteracion_Number"] = $Iteracion_Number;
           
           return view('cubo_n', $Array_Parametros);
    }
    
    public function LoadResults(){
        
            $Info = new Info(); 
            $ResultImpresion = $Info->getResults();            
            echo "<br>cantidad resultados " . count($ResultImpresion->results);
            for($index_2_control = 0; $index_2_control < count($ResultImpresion->results); $index_2_control++){
                echo "<br>Impresion resultado Operaciones: ". $ResultImpresion->results[$index_2_control];                             
            }
            
            return view('results');            
    }
    
    function UpdateOperation($X, $Y, $Z, $Value){
        
         $Info = new Info();            
         $matriz = $Info->getMatriz();
         
         $matriz->updateValue($X, $Y, $Z, $Value);                 
    }

    function QueryOperation($X1, $Y1, $Z1, $X2, $Y2, $Z2){
        
         //Procedemos a cargar la Matriz 
         $Info = new Info();            
         $matriz = $Info->getMatriz();
         
         return $matriz->query($X1, $Y1, $Z1, $X2, $Y2, $Z2);
    }
    
}
