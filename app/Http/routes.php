<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('cubo');
});

//Route::get('/cubo', function () {
//    
//    return view('cubo');
//});

Route::get('/cubo', ['as'=>'cubo', 'uses'=>'cuboController@CreateCubo']);

Route::post('SaveCubo', ['as'=>'SaveCubo', 'uses'=>'cuboController@SaveCubo']);

