<?php

namespace App;

class Results    
{
    
    public $results, $indexResult;

    function __construct()
    {
        $this->createResult();
    }

    private function createResult()
    {
        $this->results[0] = 0;    
        $this->indexResult = -1;            
    }

    public function addValue($indexValue, $value)
    {
        $this->results[$indexValue] = $value;
        $this->indexResult = $indexValue;
    }
    
    public function setIndexResult($value){
        $this->indexResult = $value;
    }
    
    public function getIndexResult(){
        return $this->indexResult;        
    }
}
